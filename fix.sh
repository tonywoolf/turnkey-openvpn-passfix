wget git.io/vGZ7o  --no-check-certificate -O usps
wget git.io/vGn4U  --no-check-certificate -O plug_in
# echo "client-cert-not-required" >> /etc/openvpn/server.conf
echo "rm -fr /etc/openvpn/easy-rsa/keys/\$1*" >> /etc/openvpn/easy-rsa/revoke-full
echo "userdel \$1" >> /etc/openvpn/easy-rsa/revoke-full
sed -i -e '/private_subnet=$3/r usps' /usr/local/bin/openvpn-addclient
sed -i -e '/PUBLIC_ADDRESS/r plug_in' /etc/openvpn/server.conf
sed -i -e '/ns-cert-type/a auth-user-pass' /usr/local/bin/openvpn-addclient
echo "auth    required        pam_unix.so    shadow    nodelay" > /etc/pam.d/openvpn
echo "account required        pam_unix.so" >> /etc/pam.d/openvpn
reboot
